import os
from pathlib import Path
import seaborn as sns

ROOT_DIR = Path(
    os.getenv(
        "ROOT_DIR", Path(os.path.dirname(os.path.abspath(__file__))) / ".."
    )
)

DIR2DATA = ROOT_DIR / "data"

COLORS = sns.color_palette("tab20")

LABEL_QUERY = "Pubmed query"
LABEL_VALUE = "Results per 100,000 articles"
