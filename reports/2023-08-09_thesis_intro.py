# %%
from matplotlib.lines import Line2D
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from qpubmed.constants import COLORS, DIR2DATA, LABEL_VALUE, LABEL_QUERY

"""
Causal inference is on the same scale than prognostic model
"""
# %%
pubmed_call = pd.read_csv(
    DIR2DATA
    / "query_pubmed_prediction_or_prognosis_model__causal__decision_making.csv"
)
query_keys = {
    "causality": COLORS[2],
    "prognostic model OR prediction model": COLORS[0],
    # "decision-making": COLORS[4],
}
query_lw = {
    "causality": "--",
    "prognostic model OR prediction model": "-",
}
pubmed_call
pubmed_call_pivoted = pubmed_call.melt(
    id_vars="Year",
    value_vars=query_keys.keys(),
    value_name=LABEL_VALUE,
    var_name=LABEL_QUERY,
)
# %%
fig, ax = plt.subplots(1, 1, figsize=(7, 3))
sns.lineplot(
    ax=ax,
    data=pubmed_call_pivoted.loc[
        pubmed_call_pivoted[LABEL_QUERY] == "causality"
    ],
    x="Year",
    y=LABEL_VALUE,
    hue=LABEL_QUERY,
    palette=query_keys,
    legend=False,
    linestyle=query_lw["causality"],
)
ax.tick_params(axis="y", labelcolor=query_keys["causality"])
ax2 = ax.twinx()
# Add a second line plot to the twin Axes
sns.lineplot(
    ax=ax2,
    data=pubmed_call_pivoted.loc[
        pubmed_call_pivoted[LABEL_QUERY] != "causality"
    ],  # Provide your data here
    x="Year",  # Adjust x and y columns accordingly
    y=LABEL_VALUE,
    hue=LABEL_QUERY,  # Choose the color for the second line plot
    palette=query_keys,
    legend=False,
    linestyle=query_lw["prognostic model OR prediction model"],
)
# ax2.set_ylabel("Second Y-axis Label")
ax2.tick_params(
    axis="y", labelcolor=query_keys["prognostic model OR prediction model"]
)

plt.legend(
    title=LABEL_QUERY,
    handles=[
        Line2D(
            [0],
            [0],
            color=query_keys[k],
            linestyle=query_lw[k],
            label=k,
        )
        for k in query_keys.keys()
    ],
    ncols=2,
    loc="upper center",
    bbox_to_anchor=(0.5, 1.25),
)
dir2paper = DIR2DATA / "../../../inria/papiers/thesis_manuscript/img/chapter_1/"
dir2img = dir2paper / "pubmed_query"
plt.savefig(str(dir2img) + ".png")
plt.savefig(str(dir2img) + ".pdf")
