# Query pubmed for literature search

## Metapub 

The first example tried to leverage the Pubmed API thanks to the metapub
package. However, for general search such as the one I wanted (eg. causality or
predictive models), I needed to have the denominator of the number of pubmed
publications per year.

## Pubmed by year

I used the [pubmed-by-year](https://esperr.github.io/pubmed-by-year/) website to
retrieve aggregated data for general terms. Then I built dedicated plots in
reports.

